import java.util.LinkedList;

public class DoubleStack {

   private LinkedList<Double> list = new LinkedList();

   public static void main (String[] argum) {
      double d = DoubleStack.interpret ("35. 10. -3. + / 2.");
      System.out.println(d);
   }

   public static double interpret (String pol) {
      DoubleStack stack = new DoubleStack();
      String[] strings = pol.trim().split("\\s+");
      try {
         return stack.interpret(strings);

      } catch (RuntimeException e) {
         throw new RuntimeException("Cannot interpret RPN '" + pol + "': " + e.getMessage());
      }
   }

   DoubleStack() {
      // TODO!!! Your constructor here!
   }

   public LinkedList getList()
   {
      return list;
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();
      for(int i = list.size() - 1; i >= 0 ; i--) {
         clone.push(list.get(i));
      }
      return clone;
   }

   public boolean stEmpty() {
      return list.isEmpty();
   }

   public void push (double a) {
      list.push(a);
   }

   public double pop() {
      checkEmpty();
      return list.removeFirst();
   }

   private void checkEmpty()
   {
      if (stEmpty()) {
         throw new RuntimeException("Stack is empty, nothing to return.");
      }
   }

   private double makeCalculation(double op1, double op2, String operator)
   {
      double returnValue;
      switch (operator) {
         case "+":
            returnValue = op2 + op1;
            break;
         case "-":
            returnValue = op2 - op1;
            break;
         case "*":
            returnValue = op2 * op1;
            break;
         case "/":
            returnValue = op2 / op1;
            break;
         default:
            throw new RuntimeException("Unknown operation. Values '+', '-', '*' and '/'. Given '" + operator + "'");
      }
      return returnValue;
   }

   public void op (String s) {
      if (list.size() < 2) {
         throw new RuntimeException("Not enough elements to calculate. Need at least 2.");
      }
      double op1 = list.removeFirst();
      double op2 = list.removeFirst();

      push(makeCalculation(op1, op2, s));
   }
  
   public double tos() {
      checkEmpty();
      return list.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      return getList().equals(((DoubleStack) o).getList());
   }

   @Override
   public String toString() {
      LinkedList<Double> toStringList = new LinkedList();
      for(int i = 0; i < list.size() ; i++) {
         toStringList.push(list.get(i));
      }
      return toStringList.toString();
   }

   public double interpret(String[] strings) {
      for (String string : strings) {
         Double doubleValue = getDoubleValue(string);
         if (doubleValue != null) {
            push(doubleValue);
         } else {
            try {
               op(string);
            } catch (RuntimeException e) {
               throw new RuntimeException("There are not enough elements for calculation");
            }
         }
      }
      Double d = pop();
      if (!stEmpty()) {
         throw new RuntimeException("Stack still contains numbers which are not calculated: '" + toString() +"'");
      }
      return d;
   }

   private Double getDoubleValue(String number)
   {
      try {
         return Double.parseDouble(number);
      } catch (NumberFormatException nfe) {
         return null;
      }
   }
}

